package com.aryalrabin.consumer.database;

import com.aryalrabin.consumer.database.entities.Product;
import com.aryalrabin.consumer.database.repository.ProductRepository;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    ProductRepository productRepository;

    @Test
    @DisplayName("Save product")
    public void saveProduct() {
        Product product = Product.builder().name("apple").quantity(10).build();

        product = entityManager.persistAndFlush(product);
        assertThat(productRepository.findById(product.getId()).get().getName()).isEqualTo(product.getName());
    }

    @Test
    @DisplayName("Save product and list test ")
    public void saveProductAndList() {
        Product product = Product.builder().name("pineapple").quantity(10).build();

        product = entityManager.persistAndFlush(product);
        assertThat(productRepository.findById(product.getId()).get().getName()).isEqualTo(product.getName());

        Product finalProduct = product;
        Optional<Product> listProduct = productRepository.findAll().stream().filter(p -> p.getId().equals(finalProduct.getId())).findFirst();
        assertThat(listProduct.isPresent()).isTrue();
    }

}
