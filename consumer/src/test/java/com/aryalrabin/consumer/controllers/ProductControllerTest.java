package com.aryalrabin.consumer.controllers;

import com.aryalrabin.consumer.database.entities.Product;
import com.aryalrabin.consumer.service.ProductService;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@WebMvcTest(value = ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    Product mockProduct = Product.builder()
                                  .id(UUID.randomUUID())
                                  .createdOn(LocalDateTime.now())
                                  .name("apple")
                                  .quantity(10)
                                  .build();
    List<Product> mockProducts = Arrays.asList(mockProduct);

    @Test
    @DisplayName("get product list")
    public void getProductsListOK() throws Exception {
        Mockito.when(productService.getProducts()).thenReturn(mockProducts);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/products").accept(
                MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}
