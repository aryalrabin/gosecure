package com.aryalrabin.consumer.service;

import com.aryalrabin.consumer.database.entities.Product;
import com.aryalrabin.consumer.database.repository.ProductRepository;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductService productService;

    @Test
    @DisplayName("when product is saved it should return product")
    public void saveProduct(){
        Product mockProduct = Product.builder().name("apple").quantity(10).build();

        when(productRepository.save(any(Product.class))).thenReturn(mockProduct);

        Product product = productService.saveProduct(mockProduct);
        assertThat(product.getName()).isSameAs(mockProduct.getName());
    }
}
