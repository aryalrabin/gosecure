package com.aryalrabin.consumer.kafka;

import com.aryalrabin.consumer.database.entities.Product;
import com.aryalrabin.consumer.database.repository.ProductRepository;
import com.aryalrabin.consumer.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class Consumer {

    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    private final ProductService productService;

    public Consumer(ProductService productService) {this.productService = productService;}



    @KafkaListener(topics = "my-topic", groupId = "my-topic-group")
    public void consume(String message) throws IOException {
        logger.info(String.format("#### -> Consumed message -> %s", message));
        ObjectMapper objectMapper = new ObjectMapper();
        Product product = objectMapper.readValue(message, Product.class);
        if (product != null){
            productService.saveProduct(product);
        }

    }
}
