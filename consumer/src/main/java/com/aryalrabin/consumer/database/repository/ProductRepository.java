package com.aryalrabin.consumer.database.repository;

import com.aryalrabin.consumer.database.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Repository
@Transactional
public interface ProductRepository
        extends JpaRepository<Product, UUID>{

}
