package com.aryalrabin.consumer.controllers;


import com.aryalrabin.consumer.database.entities.Product;
import com.aryalrabin.consumer.database.repository.ProductRepository;
import com.aryalrabin.consumer.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private  final ProductService productService;

    public ProductController(ProductService productService) {this.productService = productService;}

    @GetMapping()
    public ResponseEntity getProducts(){
        return ResponseEntity.ok(productService.getProducts());
    }

}
