CREATE DATABASE consumer;

\connect consumer

CREATE TABLE public.products
(
    id uuid NOT NULL,
    name varchar(255) NOT NULL,
    quantity int NOT NULL,
    created_on timestamp NOT NULL
);