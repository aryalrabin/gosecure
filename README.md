# kafka-python-springboot-postgresql

The take home test require to create a separate docker container for 
+ Messaging Queue
+ Database
+ Producer / Publisher App
+ Consumer / Subscriber App

This project demonstrates python producer producing a message to Kafka topic. 
Springboot consumer consumes a message, logs it to log, and saves the message into database.

I will try to build everything as a separate docker container.

#### Description of container ###

For the above requirement, I have chosen below:
+ Messaging Queue -> Apache Kafka
+ Database -> PostgreSQL
+ Producer / Publisher App -> Python Django application with Swagger UI for a user to enter a data. There is an endpoint that produces a dummy data to Kafka.
+ Consumer / Subscriber App ->  Java Springboot application save the consumes message to database. Also, provides an endpoint that display all saved records.

User will be able to connect to producer with 2 endpoints  to produce a message and  verify the message is processed and saved to database. Please find the system design.
 ![](https://photos.google.com/share/AF1QipMYb5XhAfcPvOEQWvVN3Mtla3etXBTAPPtgumVFo4mHfrfxRWMiMQ2XmpoUR-JDmQ/photo/AF1QipPM7XeHehvHH0nqQmE-58BZfKK1YB4uWarQ1w3d?key=dWdjc0tRQVlKa1U5REpwLXJyZ3FoU1k3TVZ2V0J3)

#### Pre-Requisites 

In order to run above solution, please make sure you have docker and docker-compose 
https://docs.docker.com/get-docker/
https://docs.docker.com/compose/install/


#### Checkout and running details
+ clone the project: <br>
`git clone https://gitlab.com/aryalrabin/gosecure.git`

+ cd to the project: <br>
`cd kafka-python-springboot-postgresql`

+ to start the container: <br>
`docker-compose up -d`

+ to stop the container: <br>
`docker-compose down`

Once the docker containers are running you can do following below: 

+ To Produce message.There are two ways to produce.
    + via Swagger UI http://127.0.0.1:8000, There are two endpoints 
        + `GET /product` -> send a default product to Kafka
        + `POST /product` -> send a product entered by user to Kafka
    + via curl 
        + `curl -H "Content-Type: application/json" -X GET http://127.0.0.1:8000/api/product`
		+ `curl -H "Content-Type: application/json" -X POST http://127.0.0.1:8000/api/product -d '{"name": "pineapple", "quantity": 10}'`
		
+ To verify a message is consumed and stores in DB. There are two ways to verify.	
    + via PostgreSQL command line
        +  exec a command on database container <br>
        `docker exec -it db psql -U postgres`	
        +  connect to consumer database <br>
        `\c consumer`
        +  run a select query to verify a record is saved <br>
        ` select * from products;`
    
    + via endpoint at consumer endpoint springboot (GET /products) <br>
    `curl -H "Content-Type: application/json" -X GET http://127.0.0.1:9000/products | json_pp`   


### Demo APPS
The demo app is hosted on EC2 (http://3.236.122.201)
+ Please visit http://3.236.122.201:8000 to access Swagger UI to produce 
+ Please visit http://3.236.122.201:9000/products to verify the message is consumed and stored on db.
    
