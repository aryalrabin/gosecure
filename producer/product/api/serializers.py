from rest_framework import serializers
from .models import Product

class ProductDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'