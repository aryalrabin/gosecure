from .serializers import ProductDataSerializer
from .producer import Producer
from rest_framework.response import Response
from rest_framework import status, generics
import random

class ProductData(generics.GenericAPIView):
    serializer_class = ProductDataSerializer

    def get(self, request, format=None):
        serializer = ProductDataSerializer(data={'name':  'apple', 'quantity': random.randint(1, 99)})
        if serializer.is_valid():
            Producer('my-topic').send(serializer.data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, format=None):
        serializer = ProductDataSerializer(data=request.data)
        if serializer.is_valid():
            Producer('my-topic').send(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)