from django.test import TestCase
from .models import Product


class ProductTest(TestCase):
    """ Test module for Product model """

    def setUp(self):
        Product.objects.create(
            name='apple', quantity=3)
        Product.objects.create(
            name='orange', quantity=10)

    def test_quantity(self):
        product_apple = Product.objects.get(name='apple')
        product_orange = Product.objects.get(name='orange')
        self.assertEqual(
            product_apple.quantity, 3)
        self.assertEqual(
            product_orange.quantity, 10)