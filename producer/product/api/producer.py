from kafka import KafkaProducer
from kafka.errors import KafkaError
import json
import os

class Producer():

    def __init__(self, kafkatopic):
        self.kafkatopic = kafkatopic
        self.producer = KafkaProducer(bootstrap_servers=os.getenv('PY_KAFKA_BOOTSTRAPSERVERS'),
                                      value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def send(self, value):
        try:
            producer = self.producer
            producer.send(self.kafkatopic, value)
            producer.flush()
        except KafkaError as e:
            print(e)