from django.conf.urls import url
from .views import ProductData

urlpatterns = [
    url(r'^product', ProductData.as_view(), name='product'),
]