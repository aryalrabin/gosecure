import json
from rest_framework import status
from django.test import TestCase, Client
from .serializers import ProductDataSerializer
import random

# initialize the APIClient app
client = Client()

class GetProductTest(TestCase):
    """ Test module for GET  API """
    def test_get(self):
        # get API response
        response = client.get('/api/product')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class CreateNewProductTest(TestCase):
    """ Test module for inserting a new product """

    def setUp(self):
        self.valid_payload = {
            'name': 'Cake',
            'quantity': 4
        }
        self.invalid_payload = {
            'name': '',
            'age': 4
        }

    def test_create_valid_product(self):
        response = client.post('/api/product',
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_product(self):
        response = client.post('/api/product',
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)